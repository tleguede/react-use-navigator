import React, {ReactNode, useContext} from "react";


export interface Route {
    name: string
    path: string
    exact?: boolean
    component?: ReactNode
}

export interface toUrlProps {
    params?: any
    query?: any
}

export const Navigator = React.createContext([] as Route[]);

export const useNavigator = () => {
    const routes: Route[] = useContext(Navigator);

    return {
        all: (exempt?: string[]) => {

            let res: Route[] = [];

            res = res.concat(routes);

            exempt?.forEach(name => {
                // delete res[res.findIndex(route => route.name === name)];
                res = res.filter(route => {
                    return route.name !== name;
                })
            })

            return res;
        },
        only: (names: string[]) => {
            let res: Route[] = [];

            names.forEach(name => {
                let find = routes.find(route => route.name === name);

                if (find)
                    res.push(find);
            })

            return res;
        },
        getRoute: (name: string) => {
            return routes.find(route => route.name === name);
        },
        toUrl: (name: string, {params, query}: toUrlProps={}) => {
            let url = (routes.find(route => route.name === name))?.path;

            if (params) {
                for (const paramsKey in params) {
                    url = url?.replace(`:${paramsKey}`, params[paramsKey])
                }
            }

            if (query) {
                let res = '?'
                let flag = false;

                for (const queryKey in query) {
                    if (res !== '?')
                        flag = true;

                    if (flag)
                        res += '&';

                    res += `${queryKey}=${query[queryKey].toString()}`;
                }
                url += res;
            }

            return url;
        }
    };
}
